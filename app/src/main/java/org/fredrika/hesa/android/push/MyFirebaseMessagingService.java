package org.fredrika.hesa.android.push;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.fredrika.hesa.android.FredrikaApplication;
import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.activity.SettingsActivity;
import org.fredrika.hesa.android.api.model.PushData;
import org.fredrika.hesa.android.storage.SettingsHelper;
import org.fredrika.hesa.android.storage.StorageHelper;
import org.fredrika.hesa.android.storage.model.Region;
import org.fredrika.hesa.android.util.GsonSingleton;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by Sebastian Simson on 16-09-13.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Timber.d("From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Timber.d("Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Timber.d("Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        PushData pushData = GsonSingleton.getInstance().fromJson(new JSONObject(remoteMessage.getData()).toString(), PushData.class);

        sendNotification(pushData);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    private void sendNotification(PushData data) {
        if (!SettingsHelper.receiveMessagesEnabled()) {
            return;
        }

        for (Region region : StorageHelper.getRegions()) {
            if (data.isPositionInRange(region)) {
                showNotification(data);
                return;
            }
        }

        if (SettingsHelper.receiveMessagesBasedOnCurrentLocationEnabled()) {
            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Region region = new Region();

                            region.setLongitude(location.getLongitude());
                            region.setLatitude(location.getLatitude());

                            if (data.isPositionInRange(region)) {
                                showNotification(data);
                            }
                        }
                    });
        }
    }

    private void showNotification(PushData data) {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(FredrikaApplication.getContext())
                        .setSmallIcon(R.drawable.ic_stat_fredrika)
                        .setContentTitle(data.getMessageSubject())
                        .setContentText(data.getMessage())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
