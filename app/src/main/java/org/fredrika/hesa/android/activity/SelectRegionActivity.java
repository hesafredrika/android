package org.fredrika.hesa.android.activity;

import android.support.v4.app.Fragment;

import org.fredrika.hesa.android.fragment.SelectRegionFragment;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class SelectRegionActivity extends FragmentWrapperActivity {
    @Override
    public Fragment createFragment() {
        return new SelectRegionFragment();
    }
}
