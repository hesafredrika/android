package org.fredrika.hesa.android.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Sebastian Simson on 2017-02-22.
 */

public class GsonSingleton {
    private static Gson gson;

    public static Gson getInstance() {
        if (gson == null) {
            gson = new GsonBuilder().disableHtmlEscaping().create();
        }
        return gson;
    }
}
