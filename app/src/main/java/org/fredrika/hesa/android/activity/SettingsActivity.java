package org.fredrika.hesa.android.activity;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.fragment.AlertsFragment;
import org.fredrika.hesa.android.fragment.SettingsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends FragmentWrapperActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation) BottomNavigationView bottomNavigationView;

    AlertsFragment alertsFragment;
    SettingsFragment settingsFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        alertsFragment = new AlertsFragment();
        settingsFragment = new SettingsFragment();

        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        bottomNavigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public Fragment createFragment() {
        return alertsFragment;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.action_alerts:
                if (!(getCurrentFragment() instanceof AlertsFragment)) {
                    showFragment(alertsFragment);
                }
                break;
            case R.id.action_settings:
                if (!(getCurrentFragment() instanceof SettingsFragment)) {
                    showFragment(settingsFragment);
                }
                break;
        }
        return false;
    }
}