package org.fredrika.hesa.android.api.model;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringEscapeUtils;
import org.fredrika.hesa.android.storage.model.Region;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Sebastian Simson on 2017-01-18.
 */
public class Alarm  extends BaseModel {
    @SerializedName("active") boolean active;
    @SerializedName("location") List<Location> locations;
    @SerializedName("message") String message;
    @SerializedName("subject") String subject;

    public boolean isActive() {
        return active;
    }

    public String getMessage() {
        //return StringEscapeUtils.unescapeJava(message);
        return message;
    }

    public String getSubject() {
        //return StringEscapeUtils.unescapeJava(subject);
        return subject;
    }

    public Location getLocation() {
        if (locations != null && !locations.isEmpty()) {
            return locations.get(0);
        }
        return null;
    }

    public boolean isPositionInRange(Region region) {
        for (Location location : locations) {
            double ix = location.getBoundingBox().get(0);
            double iy = location.getBoundingBox().get(2);
            double ax = location.getBoundingBox().get(1);
            double ay = location.getBoundingBox().get(3);
            double x = region.getLatitude();
            double y = region.getLongitude();

            if (ix <= x && x <= ax && iy <= y && y <= ay) {
                return true;
            }
        }

        return false;
    }
}
