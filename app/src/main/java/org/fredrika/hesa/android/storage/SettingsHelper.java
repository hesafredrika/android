package org.fredrika.hesa.android.storage;

import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import org.fredrika.hesa.android.FredrikaApplication;
import org.fredrika.hesa.android.storage.model.Region;

/**
 * Created by Sebastian Simson on 2017-11-27.
 */

public class SettingsHelper {
    public static boolean receiveMessagesEnabled() {
        return getBoolean("notification");
    }

    public static boolean receiveMessagesBasedOnCurrentLocationEnabled() {
        return getBoolean("location");
    }

    private static boolean getBoolean(String key) {
        return getPreferences().getBoolean(key, false);
    }

    public static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(FredrikaApplication.getContext());
    }

    public static Region getCurrentRegion() {
        return null;
    }
}
