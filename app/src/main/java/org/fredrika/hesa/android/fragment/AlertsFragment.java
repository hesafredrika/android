package org.fredrika.hesa.android.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.adapter.AlarmAdapter;
import org.fredrika.hesa.android.api.API;
import org.fredrika.hesa.android.api.model.Alarm;
import org.fredrika.hesa.android.storage.StorageHelper;
import org.fredrika.hesa.android.storage.model.Region;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class AlertsFragment extends Fragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.list) ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alerts, container, false);

        ButterKnife.bind(this, view);

        API.getService().getAlarms().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapIterable(items -> items)
                .filter(item -> item.isActive())
                .filter(alarm -> {
                    for (Region region : StorageHelper.getRegions()) {
                        if (alarm.isPositionInRange(region)) {
                            return true;
                        }
                    }
                    /*if (SettingsHelper.receiveMessagesBasedOnCurrentLocationEnabled()) {
                        Region region = SettingsHelper.getCurrentRegion();
                        if (alarm.isPositionInRange(region)) {
                            return true;
                        }
                    }*/
                    return false;
                })
                .toList()
                .subscribe(alarms -> {
                    AlarmAdapter alarmAdapter = new AlarmAdapter(AlertsFragment.this.getContext(), alarms);
                    listView.setAdapter(alarmAdapter);
                });

        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Alarm alarm = (Alarm) adapterView.getAdapter().getItem(i);

        if (alarm.getId() != 0) {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(getActivity(), Uri.parse("https://hesafredrika.se/message/" + alarm.getId()));
        }
    }
}
