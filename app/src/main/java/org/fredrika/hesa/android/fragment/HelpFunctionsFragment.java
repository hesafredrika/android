package org.fredrika.hesa.android.fragment;

import android.os.Bundle;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.util.Log;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.api.API;
import org.fredrika.hesa.android.api.model.Volenteer;
import org.fredrika.hesa.android.storage.StorageHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class HelpFunctionsFragment extends BasePreferenceFragment {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preference_screen);

        final PreferenceCategory category = new PreferenceCategory(getContext());
        category.setTitle(R.string.preference_help_category_title);

        getPreferenceScreen().addPreference(category);

        API.getService().getVolenteers().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(volenteers -> {
                    Log.e("Volenteers", volenteers.toString());

                    StorageHelper.setHelpCategories(volenteers);

                    for (Volenteer volenteer : volenteers) {
                        addCheckBoxPreference(volenteer.getName(), volenteer.getNote(), "" + volenteer.getId(), category, StorageHelper.isHelpCategoryActive(volenteer.getId()));
                    }
                });
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference instanceof CheckBoxPreference) {
            Long id = Long.parseLong(preference.getKey());
            StorageHelper.setHelpCategoryActive(id, (Boolean) newValue);
            return true;
        }
        return false;
    }
}
