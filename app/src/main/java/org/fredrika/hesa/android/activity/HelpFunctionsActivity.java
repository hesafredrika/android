package org.fredrika.hesa.android.activity;

import android.support.v4.app.Fragment;

import org.fredrika.hesa.android.fragment.HelpFunctionsFragment;

/**
 * Created by slebbo on 15-11-20.
 */
public class HelpFunctionsActivity extends FragmentWrapperActivity {
    @Override
    public Fragment createFragment() {
        return new HelpFunctionsFragment();
    }
}
