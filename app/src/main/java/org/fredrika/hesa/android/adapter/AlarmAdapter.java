package org.fredrika.hesa.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.api.model.Alarm;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class AlarmAdapter extends BaseAdapter {

    private final List<Alarm> items;
    private final Context context;

    public AlarmAdapter(Context context, List<Alarm> alertList) {
        this.items = alertList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Alarm getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.alarm_item, parent, false);

            viewHolder = new ViewHolder();
            ButterKnife.bind(viewHolder, convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Alarm alarm = getItem(position);

        viewHolder.subject.setText(alarm.getSubject());
        viewHolder.message.setText(alarm.getLocation().getDisplayName());

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.subject) TextView subject;
        @BindView(R.id.message) TextView message;
    }

}
