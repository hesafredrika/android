package org.fredrika.hesa.android.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */
public class BaseModel {
    @SerializedName("id") long id;
    @SerializedName("created") String created;
    @SerializedName("updated") String updated;
    @SerializedName("url") String url;
    @SerializedName("name") String name;
    @SerializedName("note") String note;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
