package org.fredrika.hesa.android.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.activity.HelpFunctionsActivity;
import org.fredrika.hesa.android.activity.SelectRegionActivity;
import org.fredrika.hesa.android.api.model.Volenteer;
import org.fredrika.hesa.android.storage.StorageHelper;
import org.fredrika.hesa.android.storage.model.Region;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class SettingsFragment extends BasePreferenceFragment implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {
    private PreferenceCategory regionCategory;
    private PreferenceCategory helpCategory;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_headers);

        regionCategory = (PreferenceCategory) findPreference("region_category");
        helpCategory = (PreferenceCategory) findPreference("help_category");
    }

    @Override
    public void onResume() {
        super.onResume();

        update();
    }

    private void update() {
        regionCategory.removeAll();
        addPreference(null, getString(R.string.preference_message_location_summary), regionCategory);
        for (Region region : StorageHelper.getRegions()) {
            addPreference(region.getName(), "" + region.getRadius() + " m", regionCategory).setOnPreferenceClickListener(preference -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.remove_region_title)
                        .setPositiveButton(R.string.yes, (dialog, id) -> {
                            StorageHelper.removeRegion(region);
                            update();
                        })
                        .setNegativeButton(R.string.no, null);
                builder.create().show();

                return true;
            });
        }
        addPreference(getString(R.string.preference_location_add_more_title), null,  R.drawable.ic_add, "add_more_regions", regionCategory);

        helpCategory.removeAll();
        addPreference(null, getString(R.string.preference_help_category_summary), helpCategory);
        for (Volenteer volenteer : StorageHelper.getHelpCategories()) {
            if (StorageHelper.isHelpCategoryActive(volenteer.getId())) {
                addPreference(volenteer.getName(), volenteer.getNote(), helpCategory);
            }
        }
        addPreference(getString(R.string.preference_help_add_more_title), null,  R.drawable.ic_add, "add_more_help_types", helpCategory);

        findPreference("add_more_regions").setOnPreferenceClickListener(this);
        findPreference("add_more_help_types").setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        String stringValue = value.toString();

        // For all other preferences, set the summary to the value's
        // simple string representation.
        preference.setSummary(stringValue);

        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals("add_more_regions")) {
            startActivity(new Intent(getContext(), SelectRegionActivity.class));
        } else if (preference.getKey().equals("add_more_help_types")) {
            startActivity(new Intent(getContext(), HelpFunctionsActivity.class));
        }
        return false;
    }
}
