package org.fredrika.hesa.android.storage.model;

import android.location.Location;

/**
 * Created by Sebastian Simson on 2017-02-22.
 */
public class Region {
    private double longitude;
    private double latitude;
    private int radius;
    private String name;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Region) {
            return getLongitude() == ((Region) obj).getLongitude() && getLatitude() == ((Region) obj).getLatitude() && getRadius() == ((Region) obj).getRadius() && ((getName() == null && ((Region) obj).getName() == null) || getName().equals(((Region) obj).getName()));
        }
        return super.equals(obj);
    }
}
