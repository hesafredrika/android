package org.fredrika.hesa.android.api.model;

import android.util.Base64;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.fredrika.hesa.android.storage.model.Region;
import org.fredrika.hesa.android.util.GsonSingleton;

import java.util.ArrayList;

/**
 * Created by Sebastian Simson on 2017-12-01.
 */

public class PushData {
    @SerializedName("location_name")
    String locationName;
    @SerializedName("description")
    String description;
    @SerializedName("message_id")
    String messageId;
    @SerializedName("topic_name")
    String topicName;
    @SerializedName("lat")
    double latitude;
    @SerializedName("lon")
    double longitude;
    @SerializedName("message")
    String message;
    @SerializedName("message_subject")
    String messageSubject;
    @SerializedName("place_id")
    int placeId;
    @SerializedName("location")
    String location;

    public String getLocationName() {
        return locationName;
    }

    public String getDescription() {
        return description;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getTopicName() {
        return topicName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getMessage() {
        return new String(Base64.decode(message, Base64.DEFAULT));
    }

    public String getMessageSubject() {
        return messageSubject;
    }

    public int getPlaceId() {
        return placeId;
    }

    public ArrayList<Double> getLocation() {
        return GsonSingleton.getInstance().fromJson(location.replace("\"", ""), new TypeToken<ArrayList<Double>>(){}.getType());
    }

    public boolean isPositionInRange(Region region) {
        ArrayList<Double> location = getLocation();
        double ix = location.get(0);
        double iy = location.get(2);
        double ax = location.get(1);
        double ay = location.get(3);
        double x = region.getLatitude();
        double y = region.getLongitude();

        if (ix <= x && x <= ax && iy <= y && y <= ay) {
            return true;
        }

        return false;
    }
}
