package org.fredrika.hesa.android.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SeekBar;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.patloew.rxlocation.RxLocation;

import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.storage.StorageHelper;
import org.fredrika.hesa.android.storage.model.Region;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public class SelectRegionFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, SeekBar.OnSeekBarChangeListener {
    private static final int PERMISSIONS_REQUEST_LOCATION = 1001;
    private int progressMin;

    @BindView(R.id.radius)
    SeekBar radius;

    private GoogleMap googleMap;
    private Circle circle;
    private Marker marker;
    private FusedLocationProviderClient mFusedLocationClient;
    private RxLocation rxLocation;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        progressMin = getResources().getInteger(R.integer.region_radius_min);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        setHasOptionsMenu(true);

        rxLocation = new RxLocation(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        FrameLayout layout = (FrameLayout) layoutInflater.inflate(R.layout.fragment_select_region, container, false);

        ButterKnife.bind(this, layout);

        View view = super.onCreateView(layoutInflater, container, bundle);
        layout.addView(view, 0);

        getMapAsync(this);

        radius.setOnSeekBarChangeListener(this);

        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_region, menu);
    }

    private Observable<List<Address>> getAddressFromLocation(double latitude, double longitude) {
        return rxLocation.geocoding().fromLocation(latitude, longitude, 1).toObservable()
                .subscribeOn(Schedulers.io());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                if (marker != null) {
                    getAddressFromLocation(marker.getPosition().latitude, marker.getPosition().longitude).observeOn(AndroidSchedulers.mainThread()).subscribe(addresses -> {
                        if (addresses.size() > 0) {
                            Region region = new Region();
                            region.setLongitude(marker.getPosition().longitude);
                            region.setLatitude(marker.getPosition().latitude);

                            Address address = addresses.get(0);
                            ArrayList<String> addressFragments = new ArrayList<String>();

                            // Fetch the address lines using getAddressLine,
                            // join them, and send them to the thread.
                            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                                addressFragments.add(address.getAddressLine(i));
                            }

                            region.setName(TextUtils.join(System.getProperty("line.separator"), addressFragments));
                            region.setRadius(getRadius());

                            StorageHelper.addRegion(region);
                            getActivity().finish();
                        }
                    });
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Timber.d("onMapReady");

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION }, PERMISSIONS_REQUEST_LOCATION);
            return;
        } else {
            this.googleMap = googleMap;
            googleMap.setMyLocationEnabled(true);

            mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null) {
                        LatLng locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, 11));

                        setMarker(locationLatLng);
                    }
                }
            });

            googleMap.setOnMapClickListener(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getMapAsync(this);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        setMarker(latLng);
    }

    private void setMarker(LatLng latLng) {
        if (marker == null) {
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("Location")
                    .snippet("Selected location"));

            circle = googleMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(getRadius())
                    .strokeColor(ContextCompat.getColor(getContext(), R.color.map_circle_stroke))
                    .strokeWidth(1)
                    .fillColor(ContextCompat.getColor(getContext(), R.color.map_circle_fill)));
        }
        marker.setPosition(latLng);
        circle.setCenter(latLng);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (circle != null) {
            circle.setRadius(getRadius());
        }
    }

    private int getRadius() {
        return progressMin + radius.getProgress();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
