package org.fredrika.hesa.android.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.reflect.TypeToken;

import org.fredrika.hesa.android.FredrikaApplication;
import org.fredrika.hesa.android.R;
import org.fredrika.hesa.android.api.model.Location;
import org.fredrika.hesa.android.api.model.Topic;
import org.fredrika.hesa.android.api.model.Volenteer;
import org.fredrika.hesa.android.storage.model.Region;
import org.fredrika.hesa.android.util.GsonSingleton;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Sebastian Simson on 2017-02-22.
 */
public class StorageHelper {
    private static final String HELP_CATEGORIES_KEY = "help_categories_key";
    private static final String ACTIVE_HELP_CATEGORIES_KEY = "active_help_categories_key";
    private static final String REGIONS_KEY = "regions_key";

    public static void addRegion(Region region) {
        ArrayList<Region> regions = getRegions();

        regions.add(region);

        setRegions(regions);
    }

    private static void setRegions(ArrayList<Region> regions) {
        save(regions, REGIONS_KEY);
    }

    public static ArrayList<Region> getRegions() {
        Type type = new TypeToken<ArrayList<Region>>(){}.getType();

        ArrayList<Region> regions = get(REGIONS_KEY, type);
        if (regions == null) {
            return new ArrayList<Region>();
        }
        return regions;
    }

    public static void addHelpCategoryActive(long helpCategoryId) {
        ArrayList<Long> activeTopics = getActiveHelpCategories();

        activeTopics.add(helpCategoryId);

        setActiveHelpCategories(activeTopics);
    }

    private static void removeHelpCategoryActive(long helpCategoryId) {
        ArrayList<Long> activeTopics = getActiveHelpCategories();

        activeTopics.remove(helpCategoryId);

        setActiveHelpCategories(activeTopics);
    }

    public static void setHelpCategoryActive(long id, boolean active) {
        if (active) {
            if (!isHelpCategoryActive(id)) {
                addHelpCategoryActive(id);
            }
        } else {
            if (isHelpCategoryActive(id)) {
                removeHelpCategoryActive(id);
            }
        }
    }

    private static void setActiveHelpCategories(ArrayList<Long> activeTopics) {
        save(activeTopics, ACTIVE_HELP_CATEGORIES_KEY);
    }

    public static ArrayList<Long> getActiveHelpCategories() {
        Type type = new TypeToken<ArrayList<Long>>(){}.getType();

        ArrayList<Long> activeTopics = get(ACTIVE_HELP_CATEGORIES_KEY, type);
        if (activeTopics == null) {
            return new ArrayList<Long>();
        }
        return activeTopics;
    }

    public static ArrayList<Volenteer> getHelpCategories() {
        Type type = new TypeToken<ArrayList<Volenteer>>(){}.getType();

        ArrayList<Volenteer> topics = get(HELP_CATEGORIES_KEY, type);
        if (topics == null) {
            return new ArrayList<Volenteer>();
        }
        return topics;
    }

    public static void setHelpCategories(ArrayList<Volenteer> volenteers) {
        save(volenteers, HELP_CATEGORIES_KEY);
    }

    public static boolean isHelpCategoryActive(long id) {
        ArrayList<Long> activeTopics = getActiveHelpCategories();
        return activeTopics.contains(id);
    }

    private static SharedPreferences getPreferences() {
        return FredrikaApplication.getContext().getSharedPreferences(FredrikaApplication.getContext().getString(R.string.storage_preference_file_key), Context.MODE_PRIVATE);
    }

    private static void save(Object object, String key) {
        SharedPreferences.Editor editor = getPreferences().edit();

        editor.putString(key, GsonSingleton.getInstance().toJson(object));

        editor.apply();
    }

    private static <T> T get(String key, Type type) {
        return GsonSingleton.getInstance().fromJson(getPreferences().getString(key, null), type);
    }

    public static void removeRegion(Region region) {
        ArrayList<Region> regions = getRegions();

        regions.remove(region);

        setRegions(regions);
    }
}
