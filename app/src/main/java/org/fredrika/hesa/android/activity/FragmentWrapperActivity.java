package org.fredrika.hesa.android.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import org.fredrika.hesa.android.R;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public abstract class FragmentWrapperActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResourceId());

        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            showFragment(createFragment(), getIntent().getExtras());
        }
    }

    protected void showFragment(Fragment fragment) {
        showFragment(fragment, null);
    }

    protected void showFragment(Fragment fragment, Bundle extras) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (extras != null) {
            fragment.setArguments(extras);
        }
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.commit();
    }

    protected Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.content);
        return fragment;
    }

    protected int getLayoutResourceId() {
        return R.layout.activity_fragment_wrapper;
    }

    public abstract Fragment createFragment();
}
