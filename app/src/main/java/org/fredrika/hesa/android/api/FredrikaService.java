package org.fredrika.hesa.android.api;

import org.fredrika.hesa.android.api.model.Alarm;
import org.fredrika.hesa.android.api.model.Topic;
import org.fredrika.hesa.android.api.model.Volenteer;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Sebastian Simson on 2017-01-18.
 */

public interface FredrikaService {
    @GET("volunteers")
    Observable<ArrayList<Volenteer>> getVolenteers();

    @GET("alarms")
    Observable<ArrayList<Alarm>> getAlarms();

    @GET("alarms/{id}")
    Observable<ArrayList<Alarm>> getAlarm(int id);

    @GET("topics")
    Observable<ArrayList<Topic>> getTopics();
}
