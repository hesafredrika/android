package org.fredrika.hesa.android;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import org.fredrika.hesa.android.api.API;
import org.fredrika.hesa.android.api.model.Volenteer;
import org.fredrika.hesa.android.storage.StorageHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by slebbo on 15-11-23.
 */
public class FredrikaApplication extends Application {

    private static FredrikaApplication application;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        FirebaseMessaging.getInstance().subscribeToTopic("all");

        API.getService().getVolenteers().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(volenteers -> {
                    StorageHelper.setHelpCategories(volenteers);

                    for (Volenteer volenteer : volenteers) {
                        FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + volenteer.getId());
                    }
                });
    }

    public static Context getContext() {
        return application.getApplicationContext();
    }
}
