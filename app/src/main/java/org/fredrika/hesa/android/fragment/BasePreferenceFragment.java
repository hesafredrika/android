package org.fredrika.hesa.android.fragment;

import android.os.Bundle;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;

/**
 * Created by Sebastian Simson on 2017-01-19.
 */

public abstract class BasePreferenceFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {
    protected void addCheckBoxPreference(String title, String summary, String key, PreferenceCategory category, boolean checked) {
        CheckBoxPreference checkBoxPref = new CheckBoxPreference(getContext());
        checkBoxPref.setTitle(title);
        checkBoxPref.setSummary(summary);
        checkBoxPref.setChecked(checked);
        checkBoxPref.setIcon(null);
        checkBoxPref.setKey(key);
        checkBoxPref.setOnPreferenceChangeListener(this);

        category.addPreference(checkBoxPref);
    }

    protected void addCheckBoxPreference(String title, String summary, String key, PreferenceCategory category) {
        addCheckBoxPreference(title, summary, key, category, true);
    }

    protected Preference addPreference(String title, String summary, PreferenceCategory category) {
        return addPreference(title, summary, 0, null, category);
    }

    protected Preference addPreference(String title, String summary, int icon, String key, PreferenceCategory category) {
        Preference preference = new Preference(getContext());
        preference.setTitle(title);
        preference.setSummary(summary);
        if (icon != 0) {
            preference.setIcon(icon);
        }
        preference.setKey(key);

        category.addPreference(preference);

        return preference;
    }

    @Override
    abstract public boolean onPreferenceChange(Preference preference, Object newValue);
}
